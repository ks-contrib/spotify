# spotify

A proprietary music streaming service

[Homepage](https://www.spotify.com)

Required package from KCP:
```
kcp -di curl-kcp
```

### Install:
```
kcp -i spotify
```
